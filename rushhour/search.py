import operator
import random

import pygame
import pprint
import copy
import time
import sys
import heapq

sys.path.append(".")
# from rushHour import *
from car import Car
from common import Common


class Node:
    def __init__(self, parent, move, state):
        self.parent = parent
        self.move = move
        self.state = state


class ValuedNode(Node):
    path = list()

    def __init__(self, parent, move, state, target_car):
        super().__init__(parent, move, state)
        self.target_car = target_car
        # self.get_path()
        self.depth = self.parent.depth + 1 if self.parent is not None else 0
        g_score = self.g_func()
        h_score = self.heuristic()
        # print(f"g score:{g_score}, h score:{h_score}")
        # pprint.pprint(self.path)
        self.eval = g_score + h_score

    def get_path(self):
        if self.parent is not None:
            # self.path = copy.deepcopy(self.parent.path)
            # self.path.append(self.state)
            self.path = self.parent.path + [self.state]
        else:
            self.path = [self.state]

    def g_func(self):
        # return len(self.path)
        return self.depth

    def heuristic(self):
        eval = 0
        target_found = False
        for i in range(0, 6):
            curr_spot = self.state[2][i]
            # print(curr_spot + " " + self.target_car)
            if curr_spot == self.target_car:
                target_found = True
            if not target_found:
                continue
            else:
                if curr_spot != self.target_car and curr_spot != ".":
                    eval += 1
        return eval

    def __cmp__(self, other):
        if self.eval < other.eval:
            return -1
        elif self.eval == other.eval:
            return 0
        else:
            return 1

    def __eq__(self, other):
        return self.state == other.state

    def __lt__(self, other):
        return self.eval < other.eval

    def __le__(self, other):
        return self.eval <= other.eval

    def __gt__(self, other):
        return self.eval > other.eval

    def __ge__(self, other):
        return self.eval >= other.eval


challenges = {}
cars = {}
outer_edge_width = 30
max_car_length = 3
target_car = None


# load all the challenges from file
def load_challenges():
    global challenges
    challenges = Common.load_challenges()


def init_cars(config):
    global cars, target_car
    cars = {}
    for k, v in config.items():
        print(k)
        carImg = pygame.image.load("img/car_{}.png".format(k))  # returns a surface
        car = Car(k, int(v["length"]), Common.translate_j_to_x(v["j"]), Common.translate_i_to_y(v["i"]), v["direction"],
                  carImg,
                  board_offset=outer_edge_width)
        cars[k] = car
    target_car = cars[next(iter(cars))]


def init_challenge(i):
    challenge = challenges[str(i)]
    init_cars(challenge["config"])


def init_lot_state() -> list:
    lot_state = [["." for i in range(6)] for j in range(6)]
    for _ in range(max_car_length):
        lot_state[2].append(".")  # Give additional space for the row where the exit is located
    return lot_state


def get_current_lot_state() -> list:
    current_lot_state = init_lot_state()
    for car in cars.values():
        # j = Common.translate_x_to_j(car.collider.x) - 1
        # i = Common.translate_y_to_i(car.collider.y) - 1
        j = car.j
        i = car.i
        if car.direction == "H":
            for k in range(car.length):
                current_lot_state[i][j + k] = car.alias
        elif car.direction == "V":
            for k in range(car.length):
                current_lot_state[i + k][j] = car.alias
    return current_lot_state


def locate_a_car(state, car):
    i = -1
    j = -1
    # print(f"finding {car}")
    for ii in range(6):
        if ii != 2:
            for jj in range(6):
                # print(curr_state[ii][jj])
                if state[ii][jj] == car:
                    i = ii
                    j = jj
                    break
            else:
                continue
            break
        else:
            for jj in range(9):
                # print(curr_state[ii][jj])
                if state[ii][jj] == car:
                    i = ii
                    j = jj
                    break
            else:
                continue
            break

    if i == -1 or j == -1:
        raise ValueError(f"car {car} not found on the board")
    return i, j


def getPossibleOneSpotMovesForOneCar(curr_state, cars, car):
    possibleMoves = list()
    i, j = locate_a_car(curr_state, car)
    # print(f"{i}{j}")
    if cars[car].direction == "H":
        # move to its right by 1 spot
        if (i != 2 and j + (cars[car].length - 1) + 1 <= 5) or (i == 2 and j + (cars[car].length - 1) + 1 <= 6):  # in the possible movement search, right bound j index need not to be set to 8. Moving beyong index 6 is considered to be an end.
            if curr_state[i][j + (cars[car].length - 1) + 1] == ".":
                possibleMoves.append((car, "H", 1))
        # move to its left by 1 spot
        if j - 1 >= 0:
            if curr_state[i][j - 1] == ".":
                possibleMoves.append((car, "H", -1))
    else:
        # down
        if i + (cars[car].length - 1) + 1 <= 5:
            if curr_state[i + (cars[car].length - 1) + 1][j] == ".":
                possibleMoves.append((car, "V", 1))
        # up
        if i - 1 >= 0:
            if curr_state[i - 1][j] == ".":
                possibleMoves.append((car, "V", -1))
    return possibleMoves


def getPossibleMovesForOneCar(curr_state, cars, car):
    possibleMoves = list()
    i, j = locate_a_car(curr_state, car)
    # print(f"{i}{j}")
    if cars[car].direction == "H":
        leftSpace = j
        rightSpace = 6 - j - cars[car].length if i != 2 else 7 - j - cars[car].length
        # move left
        if leftSpace > 0:
            for s in range(1, leftSpace + 1):
                if curr_state[i][j - s] == ".":
                    possibleMoves.append((car, "H", -s))
                else:
                    break  # break when it hits another car
        # move right
        if rightSpace > 0:
            for s in range(1, rightSpace + 1):
                if curr_state[i][j + (cars[car].length - 1) + s] == ".":
                    possibleMoves.append((car, "H", s))
                else:
                    break  # break when it hits another car
    else:
        upSpace = i
        downSpace = 6 - i - cars[car].length
        # up
        if upSpace > 0:
            for s in range(1, upSpace + 1):
                if curr_state[i - s][j] == ".":
                    possibleMoves.append((car, "V", -s))
                else:
                    break  # break when it hits another car
        # down
        if downSpace > 0:
            for s in range(1, downSpace + 1):
                if curr_state[i + (cars[car].length - 1) + s][j] == ".":
                    possibleMoves.append((car, "V", s))
                else:
                    break  # break when it hits another car
    return possibleMoves


def getPossibleOneSpotMoves(currState, cars):
    possibleMoves = list()
    for car in cars.keys():
        pmfoc = getPossibleOneSpotMovesForOneCar(currState, cars, car)
        for pm in pmfoc:
            possibleMoves.append(pm)
    return possibleMoves


def getPossibleMoves(currState, cars):
    possibleMoves = list()
    for car in cars.keys():
        pmfoc = getPossibleMovesForOneCar(currState, cars, car)
        for pm in pmfoc:
            possibleMoves.append(pm)
    return possibleMoves


def slide(cars, state, move):
    new_state = copy.deepcopy(state)
    car = move[0]
    i, j = locate_a_car(new_state, car)
    if move[1] == "H":
        # if move[2] == 1:
        #     new_state[i][j] = "."
        #     new_state[i][j + (cars[car].length - 1) + 1] = car
        # elif move[2] == -1:
        #     new_state[i][j + (cars[car].length - 1)] = "."
        #     new_state[i][j-1] = car
        # else:
        #     raise ValueError("invalid movement distance for one heuristic step")
        for k in range(cars[car].length):
            new_state[i][j + k] = "."
        for k in range(cars[car].length):
            new_state[i][j + move[2] + k] = car
    elif move[1] == "V":
        # if move[2] == 1:
        #     new_state[i][j] = "."
        #     new_state[i + (cars[car].length - 1) + 1][j] = car
        # elif move[2] == -1:
        #     new_state[i + (cars[car].length - 1)][j] = "."
        #     new_state[i-1][j] = car
        # else:
        #     raise ValueError("invalid movement distance for one heuristic step")
        for k in range(cars[car].length):
            new_state[i + k][j] = "."
        for k in range(cars[car].length):
            new_state[i + move[2] + k][j] = car
    else:
        raise ValueError("invalid movement direction for a car")
    return new_state


def is_end(state, car):
    if state[2][6] == car:
        return True
    else:
        return False


def trace_path(node):
    path = list()
    n_moves = 0
    path.append(node.state)
    while isinstance(node, Node) and node.parent is not None:
        parent = node.parent
        move = node.move
        n_moves += 1
        path.append(move)
        path.append(parent.state)
        node = node.parent
    # else:
    #     path.append(node.state)
    path.reverse()
    path[len(path) - 2] = (path[len(path) - 2][0], path[len(path) - 2][1], path[len(path) - 2][2] + 2)
    return path, n_moves


def bfs(startState, cars, target_car):
    print("Starting bfs")
    solution = list()
    n_moves = -1
    n_node_expanded = 0
    queue = list()
    startNode = Node(None, None, startState)
    queue.append(startNode)
    seen = list()
    seen.append(startState)
    while len(queue) > 0:
        node = queue.pop(0)  # pop the next node to visit
        n_node_expanded += 1
        pprint.pprint(node.state)
        if is_end(node.state, target_car.alias):
            solution, n_moves = trace_path(node)
            break
        possibleMoves = getPossibleMoves(node.state, cars)
        # possibleMoves = getPossibleOneSpotMoves(node.state, cars)
        print(possibleMoves)
        nodes = list()
        for move in possibleMoves:
            nodes.append(Node(node, move, slide(cars, node.state, move)))
        for node in nodes:
            if node.state not in seen:  # determine if the state has been visited (seen)
                queue.append(node)
                seen.append(node.state)
        # pprint.pprint(state)
        print()
    print(f"total number of node visited = {n_node_expanded}")
    return solution, n_moves, n_node_expanded


# def dfs(startState, cars, target_car):
    # print("Starting dfs")
    # solution = list()
    # n_moves = -1
    # # Need a stack
    # stack = list()
    # stack.append(startState)
    # seen = list()  # is visited
    # seen.append(startState)
    # while len(stack) > 0:
    #     state = stack.pop()
    #     pprint.pprint(state)
    #     if is_end(state, target_car.alias):
    #         solution, n_moves = trace_path(state)
    #         break
    #     possibleMoves = getPossibleMoves(state, cars)
    #     print(possibleMoves)
    #     nodes = list()
    #     for move in possibleMoves:
    #         nodes.append(Node(state, move, slide(state, move)))
    #     for w in nodes:
    #         if w not in seen:
    #             stack.append(w)
    #             seen.append(w)
    #     # pprint.pprint(state)
    # return solution, n_moves


def a_star_search(start_state, cars, target_car):
    solution = list()
    n_moves = 0
    open = list()
    closed = list()
    n_node_expanded = 0
    # open.append(ValuedNode(None, None, start_state, target_car.alias))
    heapq.heappush(open, ValuedNode(None, None, start_state, target_car.alias))
    while len(open) > 0:
        # nxt = open.pop(0)
        nxt = heapq.heappop(open)
        n_node_expanded += 1

        if is_end(nxt.state, target_car.alias):
            solution, n_moves = trace_path(nxt)
            break

        if nxt.state in closed:
            continue
        closed.append(nxt.state)

        possibleMoves = getPossibleMoves(nxt.state, cars)
        random.shuffle(possibleMoves)
        print(possibleMoves)
        nodes = list()
        for move in possibleMoves:
            newnode = ValuedNode(nxt, move, slide(cars, nxt.state, move), target_car.alias)
            keeper = True
            for c in closed:
                if newnode.state == c: # and newnode.depth >= c.depth:
                    keeper = False
                    break
            if not keeper:
                continue

            idx_replace = -1
            # for op in open:
            for i in range(len(open)):
                if newnode.state == open[i].state:
                    # if len(newnode.path) >= len(open[i].path):
                    if newnode.depth >= open[i].depth:
                        keeper = False
                    else:
                        idx_replace = i
                    break

            if keeper:
                if idx_replace > -1:
                    open[idx_replace] = newnode
                    heapq.heapify(open)
                else:
                    # open.append(newnode)
                    heapq.heappush(open, newnode)
                pprint.pprint(newnode.state)
                print(f"eval = {newnode.eval}")
        # open.sort(key=operator.attrgetter('eval'))  # works for Python3
        # open.sort()  # works for Python3
        # pprint.pprint(state)
        print()
    print(f"total number of node visited = {n_node_expanded}")
    return solution, n_moves, n_node_expanded


if __name__ == "__main__":
    load_challenges()
    init_challenge("7")

    start_time = time.time()
    solution, n_moves_a_star, n_nodes_visited_a_star = a_star_search(get_current_lot_state(), cars, target_car)
    end_time = time.time()
    a_star_time = end_time - start_time
    print("Solution found. Time elapsed for a star search: {}".format(a_star_time))
    print(f"solution:")
    pprint.pprint(solution)
    print(f"num of node visited in A* is {n_nodes_visited_a_star}")
    print(f"The number of steps taken is {n_moves_a_star}")

    start_time = time.time()
    solution, n_moves_bfs, n_nodes_visited_bfs = bfs(get_current_lot_state(), cars, target_car)
    end_time = time.time()
    bfs_time = end_time - start_time
    print("Solution found. Time elapsed for bfs: {}".format(bfs_time))
    print(f"solution:")
    pprint.pprint(solution)
    print(f"num of node visited in bfs is {n_nodes_visited_bfs}")
    print(f"The number of steps taken is {n_moves_bfs}")

    # start_time = time.time()
    # solution = dfs(get_current_lot_state(), cars)
    # end_time = time.time()
    # dfs_time = end_time - start_time
    # print("Solution found. Time elapsed for dfs: {}".format(dfs_time))
    # print(f"solution:")
    # pprint.pprint(solution)
    #
    # print(f"bfs took {bfs_time}, dfs took {dfs_time}")

    print("Summary:")

    print("Time elapsed for a star search: {}".format(a_star_time))
    print(f"num of node visited in bfs is {n_nodes_visited_a_star}")
    print(f"The number of steps taken is {n_moves_a_star}")

    print("Time elapsed for bfs: {}".format(bfs_time))
    print(f"num of node visited in bfs is {n_nodes_visited_bfs}")
    print(f"The number of steps taken is {n_moves_bfs}")
